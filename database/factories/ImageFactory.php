<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ImageFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'url' => $this->getRandomImage()
        ];
    }

	/**
	 * Get random image.
	 *
	 * @return void
	 */
	public function getRandomImage() {
		$url = "https://source.unsplash.com/random/800x600";
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
		$newquery = curl_exec($ch);
		preg_match('/\"(.*)\"/', $newquery, $matches);
		return $matches[1];
	}
}
