<?php

namespace Database\Seeders;

use App\Models\Post;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::factory(10)->hasImage()->create();
        \App\Models\Post::factory(100)->hasCategory()->hasImage()->hasComments(10)->create();
        \App\Models\Video::factory(100)->hasComments(10)->create();
		$this->call([
			UserSeeder::class,
			CategorySeeder::class
		]);
    }
}