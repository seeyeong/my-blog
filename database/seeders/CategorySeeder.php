<?php

namespace Database\Seeders;

use Carbon\Carbon;
use App\Models\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		//make sure category is empty
		Category::truncate();

		$array = ['Personal', 'Business', 'Life Style', 'Food', 'Travel', 'Entertainment', 'Fashion', 'Others'];
		$datas = [];
		foreach ($array as $arr) {
			$datas[] = ['name'=>"$arr", 'updated_at' => Carbon::now(), 'created_at' => Carbon::now()];
		}
		Category::insert($datas);
    }
}
