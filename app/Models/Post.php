<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Post extends Model
{
	use SoftDeletes;
    use HasFactory;

	protected $guarded = [];

	/**
	 * Get all of the comments for the Post
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function comments()
	{
		return $this->morphMany(Comment::class, 'commentable');
	}

	/**
	 * Get the user's image.
	 *
	 * @return void
	 */
	public function image()
	{
		return $this->morphOne(Image::class, 'imageable');
	}
	
	/**
	 * Get the user that owns the Post
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function user()
	{
		return $this->belongsTo(User::class);
	}

	// this is a recommended way to declare event handlers
    public static function boot() {
        parent::boot();

        static::deleting(function($post) { // before delete() method call this
             $post->image()->delete();
             $post->comments()->delete();
             // do the rest of the cleanup...
        });
    }

	public function scopeFilter($query, array $filters)
	{
		$query->when($filters['search'] ?? false, fn($query, $search) => 
			$query
				->whereRaw("(`title` LIKE '%{$search}%' OR `content` LIKE '%{$search}%')")
		);

		$query->when($filters['category'] ?? false, fn($query, $category) => 
			$query
				->whereHas('category',fn($query) => $query->where('category_id', $category))
		);
	}

	/**
	 * Get the category that owns the Post
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function category()
	{
		return $this->belongsTo(Category::class);
	}
}
