<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\Image;
use Illuminate\Http\Request;
use Illuminate\Session\Store;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('post.index', [
			'posts' => Post::with(['image', 'category'])->latest()->filter(Request(['search', 'category']))->paginate()->withQueryString()
		]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // return "this is post create page";
		return view('post.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$validated = $request->validate([
			'title' => 'required|unique:posts|max:255',
			'content' => 'required',
			'image' => 'mimes:jpg,png'
		]);

		$post = new Post;
		$post->title = $request->title;
		$post->content = $request->content;
		$post->user_id = Auth::user()->id;
		$post->save();

		// check if image file
		if($request->hasFile('image')) {
			// request iamge and store to images storage
			$path = $request->image->store('images');

			// create new image into database
			$image = new Image(['url' => $path]);

			// save post and update imageable
			$post->image()->save($image);
		}

		return redirect('/post');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        $post = $post->load(['comments' => function( $query ) {
			$query->orderByDesc('id');
		}, 'image', 'user']);

		return view('post.show', ['post' => $post]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
		if(Auth::user()->id != $post->user_id) {
			return redirect('/post')->with('status', 'You Cannot Edit This Post.');
		}
        return view('post.edit', ['post' => $post->load('image')]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        $validated = $request->validate([
			'title' => ['required', 'max:255', Rule::unique('posts','title')->ignore($post)],
			'content' => 'required'
		]);

		if ($request->hasFile('image')) {
			$path = $request->file('image')->store('images');
			//delete old image
			if(isset($post->image->url)) {
				Storage::delete($post->image->url);
				//update image
				$post->image()->update(['url' => $path]);
			}else{
				$image = new Image(['url'=>$path]);
				$post->image()->save($image);
			}
		}

		$post->update($validated);

		return redirect()->back()->with('status', 'Updated Successful.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        $post->delete();
		return redirect('/post');
    }
}
