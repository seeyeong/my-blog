@extends('layouts.app')

@section('content')
	<div class="mt-4 md:mx-6 p-6">
		<div class="flex justify-end mb-4" >
			<div class="rounded border border-gray-300 bg-white w-1/3 justify-between flex flex-row" x-data="{ query: '{{Request('search')}}' }">
				<form x-ref="searchform" class="flex flex-row" action="/post" method="get" >
					<select class="rounded-l border-0" name="category" id="category" @change="$refs.searchform.submit()">
						<option class="p-4" value="0">{{__('All')}}</option>
						@foreach (App\Models\Category::all() as $category)
							<option class="p-4" value="{{$category->id}}" :selected="{{$category->id}} == {{Request('category')}}">{{$category->name}}</option>
						@endforeach
					</select>
					<input class="py-4 px-6 border-l-2 focus:outline-none w-full" name="search" id="search" placeholder="Search title here." x-model="query" />
				</form>
				<button class="pr-4 text-gray-500" @click="query=''; $nextTick(() => $refs.searchform.submit());">X</button>
			</div>
		</div>
		<div class="grid sm:grid-cols-1 gap-4 md:grid-cols-6">
			@foreach ($posts as $post)
				@if ($loop->first)
					<x-featured-post class="col-span-6" :post="$post" />
				@else
					<x-post class="{{$loop->iteration <= 3 ? 'col-span-3' : 'col-span-2 '}}" :post="$post" />
				@endif
			@endforeach
		</div>
		@if (count($posts) == 0)
			<div class="flex flex-col justify-center text-center">
				<img class="h-96 w-1/2 rounded object-cover object-center mx-auto" src="https://cdn.dribbble.com/users/9734/screenshots/3751250/illi-inbox-zero2.png" alt="no records found">
				<h1 class="text-2xl">
					No Posts Found.
				</h1>
			</div>
		@endif

		<div class="mt-4">
			{{ $posts->links() }}
		</div>
	</div>
@endsection