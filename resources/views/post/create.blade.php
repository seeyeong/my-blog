@extends('layouts.app')

@section('content')

	@if ($errors->any())
		<div class="alert alert-danger bg-red-200 px-6 py-4 mt-4 rounded text-red-500">
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif

    <form action="/post" method="POST" enctype="multipart/form-data">
		@csrf
        <div class="flex flex-col mt-4 space-y-4">
            {{-- <label class="font-medium text-lg" for="title">{{ __('Title') }}</label> --}}
			<x-label>{{__('Title')}}</x-label>
            {{-- <input class="border p-4" type="text" name="title" id="title" placeholder="Enter a post title here."> --}}
			<div class="flex flex-col" x-data="{title: ''}">
				<x-input class="py-4 px-6" name="title" id="title" placeholder="Enter a post title here." x-model="title" maxlength=255/>
				<div>
					<span class="text-gray-500" x-text="title.length"></span> / Max 255 Charaters.
				</div>
			</div>

            <label class="font-medium text-lg" for="content">{{ __('Content') }}</label>
            <textarea class=" rounded border p-4 border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50" name="content" id="content" cols="30" rows="10" placeholder="Write your post content here." ></textarea>
			
			<x-input class="py-4 px-6 bg-white" name="image" id="image" type="file" />

            <button 
				type="submit"
				class="bg-blue-600 w-36 px-6 py-4 rounded-md text-white 
				hover:bg-blue-700 
				focus:outline-none focus:ring-2 focus: ring-blue-600 focus:ring-opacity-50"
				>
				{{ __('Create Post') }}
			</button>
        </div>
    </form>
@endsection
