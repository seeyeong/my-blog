@extends('layouts.app')

@section('content')
	@if ($errors->any())
	<div class="alert alert-danger bg-red-200 px-6 py-4 mt-4 rounded text-red-500">
		<ul>
			@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
	@endif

	<form action="/post/{{$post->id}}" method="POST" enctype="multipart/form-data">
		@csrf
		@method('PATCH')
		<div class="flex flex-col mt-4 space-y-4">
			{{-- <label class="font-medium text-lg" for="title">{{ __('Title') }}</label> --}}
			<x-label>{{__('Title')}}</x-label>
			{{-- <input class="border p-4" type="text" name="title" id="title" placeholder="Enter a post title here."> --}}
			<x-input class="py-4 px-6" name="title" id="title" placeholder="Enter a post title here." value="{{old('title') ? old('title') : $post->title}}" />

			<label class="font-medium text-lg" for="content">{{ __('Content') }}</label>
			<textarea class=" rounded border p-4 border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50" name="content" id="content" cols="30" rows="10" placeholder="Write your post content here.">{{$post->content}}</textarea>
			
			@if ($post->image)
				<img src="/{{$post->image->url}}" class="w-80 h-auto"/>
			@endif
			<x-input class="py-4 px-6 bg-white" name="image" id="image" type="file" />

			<button 
				type="submit"
				class="bg-blue-600 w-36 px-6 py-4 rounded-md text-white 
				hover:bg-blue-700 
				focus:outline-none focus:ring-2 focus: ring-blue-600 focus:ring-opacity-50"
				>
				{{ __('Update Post') }}
			</button>
		</div>
	</form>
@endsection