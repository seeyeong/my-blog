@extends('layouts.app')

@section('content')
	<div class="max-w-7xl mx-auto">
		<div class="block border rounded px-6 py-4 bg-white mt-4">
			<article>
				<h1 class="title text-4xl mt-4 py-6">{{$post->title}}</h1>
				@php
					$imgurl = 'http://www.semenyihecoventure.com/images/joomlart/demo/default.jpg';
					if (isset($post->image)) {
						$imgurl = substr($post->image->url, 0, 4) == 'http' ? $post->image->url : "/{$post->image->url}";
					}
				@endphp
				<img class="h-80 w-full object-cover object-center rounded-t-lg" src="{{$imgurl}}" alt="" srcset="">
				<p class="py-4">
					{{$post->content}}
				</p>
				<h3>Author: {{$post->user->name}}</h3>
				<div class="post-date flex flex-col justify-between">
					<span class="flex-1">Created at: {{$post->created_at}}</span>
					<span class="flex-1">Last update at: {{$post->updated_at}}</span>
				</div>
				@auth
					@if (Auth::user()->id == $post->user_id)
						<div class="flex flex-row space-x-2 justify-end">
							<form action="/post/{{$post->id}}" method="post" onsubmit="return confirm('Are you sure?')">
								@csrf
								@method('DELETE')
								<x-button class="bg-red-500 hover:bg-red-700">{{__('Delete')}}</x-button>
							</form>
							<a href="/post/{{$post->id}}/edit">
								<x-button class="bg-blue-500 hover:bg-blue-700">
									{{__('Edit')}}
								</x-button> 
							</a>
						</div>
					@endif
				@endauth
			</article>
			<form class="flex flex-col" action="/post/{{$post->id}}/comments" method="POST">
				@csrf
				<div class="pb-6 border-t-2 mt-4 py-6">
					<span class="text-2xl font-bold">
						{{Auth::user()->name}}, Place your comment here.
					</span>
				</div>
				<textarea class="border rounded" name="body" id="body" cols="30" rows="10" placeholder="Wrtie some comment for the post." required></textarea>
				<div class="mt-4">
					<x-button class="w-auto">{{__('Post')}}</x-button>
				</div>
			</form>

			@foreach ($post->comments as $comment)
				<div class="{{$loop->last ? "" :"border-b-2"}} py-6">
					<article class="flex flex-row">
						<div class="flex-shrink-0">
							<img class="rounded-lg" src="https://i.pravatar.cc/100" alt="" width="60" height="60">
						</div>
						<div class="px-4">
							<h3 class="font-bold">John</h3>
							<p class="text-sm">{{$comment->body}}</p>
							<time class="font-thin font-mono text-gray-400 text-sm">{{$comment->created_at->diffForHumans()}}</time>
						</div>
					</article>
					
				</div>
			@endforeach
		</div>
	</div>
@endsection