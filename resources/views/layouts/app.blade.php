<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>
    </head>
    <body class="font-sans antialiased">
        <div class="min-h-screen">
            @include('layouts.navigation')

            <!-- Page Content -->
			<div class="max-w-7xl mx-auto">
				<!-- Notifications -->
					@if (session('status'))
						<div x-data="{open: true}" x-init="setTimeout(() => open=false, 5000)" >
							<div class="bg-green-300 rounded text-white py-6 px-4 mt-4 w-full flex flex-row justify-between" x-show="open">
								{{ session('status') }}
								<button class="pr-4" @click="open=false">X</button>
							</div>
						</div>
					@endif
				<main>
					{{-- {{ $slot }} --}}
					@yield('content')
				</main>
			</div>
        </div>
    </body>
</html>
