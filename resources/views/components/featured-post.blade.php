@props(['post'])

<article 
	{{$attributes->merge(['class' => 'rounded hover:border-gray-500 border-solid border-2 rounded-md p-2'])}}>
	<a href="/post/{{$post->id}}" class="w-full flex flex-row">
		<img class="rounded-l-md object-cover h-80 w-full" 
			src="{{isset($post->image) ? "{$post->image->url}" : 'http://www.semenyihecoventure.com/images/joomlart/demo/default.jpg'}}" 
			alt="" srcset="">
			<div class="h-auto p-4 flex flex-col justify-between w-full ">
				<div class="">
					<div class="bg-green-300 px-4 inline-flex text-center rounded-xl py-2">{{$post->category->name}}</div>
					<h3 class="font-bold text-3xl">
						{!! preg_replace("/(".Request('search').")/i", "<span class='bg-blue-300'>$1</span>", $post->title) !!}
					</h3>
					<p class="h-24 font-light mt-4 overflow-ellipsis overflow-hidden">
						{!! preg_replace("/(".Request('search').")/i", "<span class='bg-blue-300'>$1</span>", $post->content) !!}
					</p>
				</div>
				<p>
					Published <span class="text-sm font-thin mt-6">{{$post->created_at->diffForHumans()}}</span>
				</p>
			</div>
	</a>
</article>