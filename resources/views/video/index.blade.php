@extends('layouts.app')

@section('content')
	<h1>Video Page</h1>

	<div class="grid grid-cols-4 gap-4">
		@foreach ($videos as $video)
			<iframe width="560" height="315" src="{{$video->url}}" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
		@endforeach
	</div>

	<div class="py-4">
		{{-- {{$videos->links()}} --}}
	</div>
@endsection