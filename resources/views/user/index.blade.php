@extends('layouts.app')
@section('content')
	<div class="mt-4">
		<div class="flex flex-col">
			@foreach ($users as $user)
				<div class="flex flex-row bg-gray-100 rounded-xl mt-4">
					<img class="w-48 h-24 object-cover rounded-l-md" src="{{isset($user->image) ? $user->image->url : ""}}" alt="">
					<div class="pt-6 md:p-8 text-center md:text-left space-y-4">
						<figcaption class="font-medium">
							<div class="text-cyan-600">
							{{$user->name}}
							</div>
							<div class="text-gray-500">
							{{$user->email}}
							</div>
						</figcaption>
					</div>
				</div>
			@endforeach
		</div>
	</div>
@endsection